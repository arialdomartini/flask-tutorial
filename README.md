# The Flask Mega-Tutorial

This is a simple repository following the steps of the tutorial about [Flask](http://flask.pocoo.org/) by Miguel Grinberg ([link](http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world)).

## Getting started
* clone the project: `git clone git@bitbucket.org:maur8ino/flask-tutorial.git`
* create the virtualenv and activate it: `virtualenv flask3` and `source flask3/bin/activate`
* run the project: `./run.py`
