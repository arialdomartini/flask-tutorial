from flask import render_template, flash, request, redirect, url_for, session, g
from flask.ext.login import current_user, login_user, login_required, logout_user
from datetime import datetime
from app import app, db, lm, oid, models
from .forms import LoginForm, EditForm
from .models import User

@app.route('/')
@app.route('/index')
@login_required
def index():
    user_model = getattr(g, 'user', None)
    return render_template("index.html", title='Home', user=user_model)


@app.route('/login', methods=['GET', 'POST'])
@oid.loginhandler
def login():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        session['remeber_me'] = form.remember_me.data
        return oid.try_login(form.openid.data, ask_for=['nickname', 'email'])
    return render_template("login.html", title='Sign In', form=form, providers=app.config['OPENID_PROVIDERS'])


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/user/<nickname>')
@login_required
def user(nickname):
    user_model = models.User.query.filter_by(nickname=nickname).first()
    if user_model is None:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('index'))
    return render_template('user.html', user=user_model)


@app.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
    form = EditForm(g.user.nickname)
    if form.validate_on_submit():
        g.user.nickname = form.nickname.data
        g.user.about_me = form.about_me.data
        db.session.add(g.user)
        db.session.commit()
        flash('Your changes have been saved')
        return redirect(url_for('edit'))
    else:
        form.nickname.data = g.user.nickname
        form.about_me.data = g.user.about_me
        return render_template('edit.html', form=form)

from .oauth_providers.facebook import *

@lm.user_loader
def load_user(user_id):
    return models.User.query.get(int(user_id))


@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():
        g.user.last_seen = datetime.utcnow()
        db.session.add(g.user)
        db.session.commit()


@oid.after_login
def after_login(resp):
    if resp.email is None or resp.email == '':
        flash('Invalid login. Please try again.')
        return redirect(url_for('login'))
    user_model = models.User().query.filter_by(email=resp.email).first()
    if user_model is None:
        nickname = resp.nickname
        if nickname is None or nickname == '':
            nickname = resp.email.split('@')[0]
        nickname = User.make_unique_nickname(nickname)
        user_model = models.User(nickname=nickname, email=resp.email, role=models.ROLE_USER)
        db.session.add(user_model)
        # make the user follow him/herself
        db.session.add(user_model.follow(user_model))
        db.session.commit()
    remember_me = False
    if 'remember_me' is session:
        remember_me = session['remeber_me']
        session.pop('remember_me', None)
    login_user(user_model, remember=remember_me)
    return redirect(request.args.get('next') or url_for('index'))


@app.errorhandler(404)
def not_found_error():
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error():
    db.session.rollback()
    return render_template('500.html'), 500